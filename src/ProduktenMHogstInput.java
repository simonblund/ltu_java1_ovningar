import java.util.Scanner;

public class ProduktenMHogstInput {
    public static void main(String[] args){
        int tal1, tal2, tal3, produkten, hogst;
        Scanner input = new Scanner(System.in);

        // Forsta talet
        System.out.println("Ange ett värde för det första talet: ");
        tal1 = input.nextInt();
        hogst = tal1;
        //Andra talet
        System.out.println("Ange ett värde för det andra talet: ");
        tal2 = input.nextInt();
        if (tal2 > hogst){
            hogst = tal2;
        }
        // Tredje talet
        System.out.println("Ange ett värde för det tredje talet: ");
        tal3 = input.nextInt();
        if (tal3 > hogst){
            hogst = tal3;
        }

        produkten = tal1*tal2*tal3;


        System.out.println("Produkten blev:" + produkten);
        System.out.println("Högsta talet var: " + hogst);

    }

}
