public class Artikel {
    public int artikelantal;
    private String artikelnummer;
    private int kvantitet;
    private float pris;


    //Getters & Setters
    public void setArtikelnummer(String artikelnummer) {
        artikelantal++;
        this.artikelnummer = artikelnummer;
    }

    public void setKvantitet(int kvantitet) {
        this.kvantitet = kvantitet;
    }

    public void setPris(float pris) {
        this.pris = pris;
    }

    public String getArtikelnummer() {
        return artikelnummer;
    }

    public int getKvantitet() {
        return kvantitet;
    }

    public float getPris() {
        return pris;
    }
    // EO Setters & Getters

    public float calculateAmount(){
        if (pris < 0){
            setPris(0.0f);
        };
        if (kvantitet < 0){
            setKvantitet(0);
        };
        return pris*kvantitet;
    }


}// EOC Artikel
