import java.util.Scanner;

public class Kryptera {
    public static void main(String[] args){
        String sentence = "";
        String output = "";
        char[] sentencechar;
        int change = -2;


        Scanner input = new Scanner(System.in);

        System.out.println("Skriv in string för cesarschiffer");
        sentence = input.next();

        System.out.println("Hur många steg?");
        change = input.nextInt();

        sentencechar = sentence.toCharArray();
        
        for (int i = 0; i < sentencechar.length; i++){
            output += Charchange(sentencechar[i], change);

        }

        System.out.println(output);



    }
    public static char Charchange(char chr, int change){
        char character;
        int first = Character.getNumericValue(chr);
        int second = first + change;
        character = Character.forDigit(second, 34);
        return character;

    }
}
