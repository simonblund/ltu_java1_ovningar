public class RattaKod {
    public static void main(String[] args){
        System.out.println("Age with value 12");
        age(12);
        System.out.println("Age with value 68");
        age(68);
        System.out.println("End of Age tests");
        System.out.println();

        System.out.println("Start of sum_all test");
        System.out.println(sum_all());
        System.out.println("End of sum all tests");
        System.out.println();

        System.out.println("Start of sum_100 test x set to 1");
        System.out.println(sum_100(1));
        System.out.println("End of sum100 tests");
        System.out.println();

        System.out.println("Y blir infinite loop");




    }
    private static void age(int age){
        if (age >= 65){
        System.out.println("Age is greater than or equal to 65");
        }
        // Had to remove the semicolon before the if
        else {
        System.out.println("Age is less than 65");
        }
    }

    private static int sum_all(){
        int x = 1;
        int total =0;
        while(x <=10){
            total +=x;
            x++;
        }
        return total;
    }

    private static int sum_100(int x){
        int total = 0;
        while(x < 100){
            total +=x;
            x++;
        }
        return total;
    }


}
