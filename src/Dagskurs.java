import java.util.Scanner;

public class Dagskurs {
    public static void main(String[] args){
        //Variable declarations
        float kurs = 9.8f;
        float pris;
        float prisEUR;
        Scanner scan = new Scanner(System.in);

        // Kontroll av kursbehov
        System.out.println("Mata in ny dagskurs (J/N)");
        String ny = scan.next();
        if(ny.equalsIgnoreCase("J")){
            System.out.println("Mata in aktuell kurs");
            kurs = scan.nextFloat();
        }

        //Mata in pris
        System.out.println("Pris i SEK");
        pris = scan.nextFloat();

        // Beräkna prisEUR
        prisEUR = pris/kurs;
        System.out.println("Priset i euro: "+prisEUR);

    }

}
