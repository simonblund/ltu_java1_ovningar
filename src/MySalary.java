import java.util.Scanner;

public class MySalary {
    private static int grundlon = 15000;
    private static int receptionistTill = 10000;
    private static int officerTill = 17000;
    private static int chefTill= 23000;

    public static void main(String[] args){
        System.out.println("\n" +
                "\n" +
                "  __  __        ___        _                  \n" +
                " |  \\/  | _  _ / __| __ _ | | __ _  _ _  _  _ \n" +
                " | |\\/| || || |\\__ \\/ _` || |/ _` || '_|| || |\n" +
                " |_|  |_| \\_, ||___/\\__,_||_|\\__,_||_|   \\_, |\n" +
                "          |__/                           |__/ \n" +
                "\n");
        showMenu();

    }

    private static void showMenu(){
        // Visar menyvalen för typ av anställd.

        // Declare a scanner and give instructions
        Scanner sc = new Scanner(System.in);
        System.out.println("Beräkna din lön");
        System.out.println("Din titel:\nR = Receptionist\nO = Officer\nC = Chef");

            char selectMenu = sc.next(".").charAt(0);
            switch (selectMenu){
                case 'R': {
                    salaryCounter(receptionistTill);
                    break;
                }
                case 'O':{
                    salaryCounter(officerTill);

                    break;
                }
                case 'C':{
                    salaryCounter(chefTill);
                    break;
                }

                default:{
                    System.out.println("Ogiltigt menyval, försök på nytt");
                    try
                    {
                        Thread.sleep(50);
                    }
                    catch(InterruptedException ex)
                    {
                        Thread.currentThread().interrupt();
                    }
                    showMenu();
                    break;
                }
            }
    }// EOM Showmenu


    private static void salaryCounter(int titelTillagg){
        // Beräknar månadslönen och startar printoutlon.
        Scanner sc = new Scanner(System.in);
        // Fråga om jobbat extra
        System.out.println("Har du övertidstimmar? Svara i hela timmar.");
        int extratimmar = sc.nextInt();
        printoutLon(grundlon + titelTillagg + (titelTillagg*extratimmar*0.04f));
    }//EOM salaryCounter


    private static void printoutLon(float lon){
        // Printar ut lönen för månaden på skärmen.
        System.out.println();
        System.out.println("----------------------------------");
        System.out.println();
        System.out.println("Er lön är " + lon + " kronor den här månaden.");
    }

}//EOC Mysalary
