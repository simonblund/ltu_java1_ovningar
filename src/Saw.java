public class Saw {
    public static void main(String[] args){
        System.out.println("Test av triangelfunktionen 4");
        triangel(4);
        System.out.println();
        System.out.println("Test av sawtandfunktionen 4, 8");
        sawTooth(4,8);


    }
    private static void triangel(int a){
        for(int i = 0; i <= a; i++){
            String b = new String(new char[i]).replace("\0","*");
            // Tror ovanstående rad är definitionen av premature optimisation.
            System.out.println(b);
        }
    }
    private static void sawTooth(int a, int b){
        // För att understöda logisk slutledning börjar loopen på 1 istället för i<b
        for (int i = 1; i <= b; i++){
            triangel(a);
        }
    }
}
