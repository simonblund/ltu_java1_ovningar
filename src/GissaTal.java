import java.util.Scanner;

public class GissaTal {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        //slumpatal
        int secretnum = 1 + (int) (Math.random()*100);
        int guess = 0;
        int guess_index = 0;
        System.out.println("Gissa numret");

        while (guess != secretnum){
            guess_index++;
            System.out.println("Försök nr " + guess_index);
            guess = sc.nextInt();
            if (guess == secretnum){
                System.out.println("Duktigt! Du behövde "+ guess_index + " försök för att hitta numret!");
                break;
            }
            if (guess > secretnum){
                System.out.println("Din gissning var högre än numret");
            }
            else{
                System.out.println("Din gissning var lägre än numret");
            }
        }
    }
}
