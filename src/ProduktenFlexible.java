import java.util.Arrays;
import java.util.Scanner;

public class ProduktenFlexible {
    public static void main(String[] args){
        int[] tal;
        int antFakt, produkt;

        Scanner input = new Scanner(System.in);
        System.out.println("Antal faktorer?");
        antFakt = input.nextInt();
        tal = new int[antFakt];

        for (int i=0; i<antFakt; i++){
            System.out.println("faktor?");
            tal[i] = input.nextInt();
        }
        produkt = Arrays.stream(tal).reduce(1, (a, b) -> a * b); // Återigen, det finns ett lätt sätt och ett svårt sätt..
        System.out.println("Produkten är: "+ produkt);

    }
}
