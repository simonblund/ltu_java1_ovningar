public class EvenOdd {
    public static void main(String[] args){
        int sum = 0;
        for (int i = 0; i <=30; i++){
            if (isEven(i)){
                sum += i;
            }
        }
        System.out.println(sum);
    }
    private static boolean isEven(int value){
        return (value % 2) == 0;
    }
}
