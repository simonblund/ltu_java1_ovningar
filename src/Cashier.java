import java.util.ArrayList;
import java.util.Scanner;

public class Cashier {
    private static ArrayList<Artikel> order;

    public static void main(String[] args){
        newOrder();
    }
    private static void showMenu(){
        Scanner sc = new Scanner(System.in);

        // Text för menyn
        System.out.println("KASSAMENY");
        System.out.println("Gör ett menyval genom att välja ett nummer nedan");
        System.out.println("1. Starta ny order\n2. Lägg till vara i order\n3. Registrera betalning\n4.Skriv kvitto\n5. Avsluta ");

        while(true){
            int selectMenu = sc.nextInt();
            switch (selectMenu){
                case 1: {
                    newOrder();
                    break;
                }
                case 2:{
                    addArticle();
                    break;
                }
                case 3:{
                    addPayment();
                    break;
                }
                case 4:{
                    printReceipt();
                    break;
                }
                case 5:{
                    System.exit(0);
                    break;
                }
                default:{
                    System.out.println("Ogiltigt menyval");
                    break;
                }
            }
        }

    }
    private static void newOrder(){
        order = new ArrayList<Artikel>();
        showMenu();

    }

    private static float sum(){
        int sum = 0;
        for(int i = 0; i < order.size(); i++){
            sum += order.get(i).calculateAmount();
        }
        return sum;
    }

    private static void addArticle(){
        Scanner sc = new Scanner(System.in);
        Artikel article = new Artikel();

        // Setting things to the article
        System.out.print("Skriv in artikelnummer:");
        article.setArtikelnummer(sc.next());

        System.out.print("\n Skriv in pris:");
        article.setPris(sc.nextFloat());

        System.out.print("\n Skriv in kvantitet:");
        article.setKvantitet(sc.nextInt());

        System.out.println("Artikel: "+ article.getArtikelnummer()+ " antal: "+ article.getKvantitet() );

        // Add article to order arraylist
        order.add(article);

        // Return to menu
        System.out.println("Nuvarande summa för inköpen är: "+ sum() );
        System.out.println("\n \n --------------------------------------------------------------\n\n");
        showMenu();
    }
    private static void addPayment(){
        System.out.println("Summan att betala:" + sum());
        Scanner sc = new Scanner(System.in);
        System.out.println("1.Kortbetalning \n2.Kontantbetalning");
        if (sc.nextInt()==2){
            System.out.println("Mata in kontantbetalning");
            float kontant = sc.nextFloat();
            System.out.println("Kontant: " + kontant + "\n Växel: " + (kontant-sum()));
            printReceipt();

        } else {
            printReceipt();
        }
    }

    private static void printReceipt(){
        System.out.print("\n" +
                "\n" +
                " /$$                       /$$$$$$$                        /$$     /$$                              \n" +
                "| $$                      | $$__  $$                      | $$    |__/                              \n" +
                "| $$        /$$$$$$       | $$  \\ $$  /$$$$$$  /$$   /$$ /$$$$$$   /$$  /$$$$$$  /$$   /$$  /$$$$$$ \n" +
                "| $$       /$$__  $$      | $$$$$$$  /$$__  $$| $$  | $$|_  $$_/  | $$ /$$__  $$| $$  | $$ /$$__  $$\n" +
                "| $$      | $$$$$$$$      | $$__  $$| $$  \\ $$| $$  | $$  | $$    | $$| $$  \\ $$| $$  | $$| $$$$$$$$\n" +
                "| $$      | $$_____/      | $$  \\ $$| $$  | $$| $$  | $$  | $$ /$$| $$| $$  | $$| $$  | $$| $$_____/\n" +
                "| $$$$$$$$|  $$$$$$$      | $$$$$$$/|  $$$$$$/|  $$$$$$/  |  $$$$/| $$|  $$$$$$$|  $$$$$$/|  $$$$$$$\n" +
                "|________/ \\_______/      |_______/  \\______/  \\______/    \\___/  |__/ \\____  $$ \\______/  \\_______/\n" +
                "                                                                            | $$                    \n" +
                "                                                                            | $$                    \n" +
                "                                                                            |__/                    \n" +
                "\n");
        for(int i = 0; i < order.size(); i++){
            System.out.println(order.get(i).getArtikelnummer()+"- pris/st: "+ order.get(i).getPris() + " - kvantitet: "+ order.get(i).getKvantitet() + " - pris: "+ order.get(i).calculateAmount());
        }
        System.out.println("Slutsumma: " +sum());

    }
}
