
import java.util.Scanner;

public class CutnPaste {
    public static void main(String[] args){
        String hela;
        String fornamn;
        String efternamn;
        String initialer;
        String[] helasplit;

        Scanner input = new Scanner(System.in);
        System.out.println("Skriv in hela namnet i ordningen förnamn efternamn");
        hela = input.nextLine();

        // Separera delarna
        helasplit = hela.split("\\s+");
        fornamn = helasplit[0];
        efternamn = helasplit[1];

        // Skapa initialer
        initialer = fornamn.charAt(0) +"" +efternamn.charAt(0);

        // Printa informationen
        System.out.println("Ditt förnamn är "+fornamn+", som består av "+ fornamn.length()+" tecken.");
        System.out.println("Ditt efternamn är "+efternamn+", som består av "+ efternamn.length()+" tecken.");
        System.out.println("Dina initialer är "+ initialer);

    }

}
