import java.util.Scanner;

public class GissaTal2 {
    static int methodCount;
    static int counter;
    static int HighestNumber;
    static int LowestNumber = 100;
    public static void main(String[] args){
        showMenu();
    }

    public static void showMenu(){
        Scanner sc = new Scanner(System.in);
        System.out.println("\n 1. Nytt spel.\n 2. Visa statistik \n 3. Avsluta");

        while(true){
            int selectMenu = sc.nextInt();
            switch (selectMenu){
                case 1: {
                    playGame();
                    break;
                }
                case 2:{
                    statistics();
                    break;
                }
                case 3:{
                    System.exit(0);
                    break;
                }
                default:{
                    System.out.println("Ogiltigt menyval");
                    break;
                }
            }
        }
    }
    public static void checkScores(int guesscount){
        if (guesscount >HighestNumber){
            HighestNumber = guesscount;
        }
        if (guesscount<LowestNumber){
            LowestNumber = guesscount;
        }
        counter++;

    }

    public static void statistics(){
        System.out.println("Antal spelomgångar: "+counter);
        System.out.println("Högsta antal gissningar på ett tal: "+ HighestNumber);
        System.out.println("Lägst antal gissningar på ett tal: "+ LowestNumber);
    }

    public static void playGame(){
        Scanner sc = new Scanner(System.in);

        //slumpatal
        int secretnum = 1 + (int) (Math.random()*100);
        int guess = 0;
        int guess_index = 0;
        System.out.println("Gissa numret");

        while (guess != secretnum){
            guess_index++;
            System.out.println("Försök nr " + guess_index);
            guess = sc.nextInt();
            if (guess == secretnum){
                System.out.println("Duktigt! Du behövde "+ guess_index + " försök för att hitta numret!");
                checkScores(guess_index);
                showMenu();
                break;
            }
            if (guess > secretnum){
                System.out.println("Din gissning var högre än numret");
            }
            else{
                System.out.println("Din gissning var lägre än numret");
            }
        }
    }
}
