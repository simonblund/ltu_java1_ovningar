import java.util.Scanner;
public class Produkten {

    public static void main(String[] args){
        int tal1, tal2, tal3, produkten;
        Scanner input = new Scanner(System.in);
        System.out.println("Ange ett värde för det första talet: ");
        tal1 = input.nextInt();
        System.out.println("Ange ett värde för det andra talet: ");
        tal2 = input.nextInt();
        System.out.println("Ange ett värde för det tredje talet: ");
        tal3 = input.nextInt();

        produkten = tal1*tal2*tal3;
        System.out.println("Produkten blev:" + produkten);

    }

}
