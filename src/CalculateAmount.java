import java.util.Scanner;

public class CalculateAmount {
    public static void main(String[] args){
        float sum;
        float rabattkrav = 100;
        float rabatt;
        System.out.println("Skriv in köpesumma: ");
        Scanner lasin = new Scanner(System.in);
        sum = lasin.nextFloat();
        if (sum == 0){
            System.exit(1);
        }

        System.out.println("Mata in rabattprocenten utan procenttecken");
        rabatt = 1 - lasin.nextFloat()/100;
        if (sum >=rabattkrav){
            sum = (sum * rabatt);
        }
        System.out.println("Summa att betala: "+ sum);
    }

}
