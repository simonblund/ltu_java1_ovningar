public class HeltalSamling {
    public static void main(String[] argv){

        int[] heltal = {0,0,0,0,0,0,0,0,0,0};
        for (int i=0; i<10; i++){
            heltal[i] = (int) (Math.random()*10);
        }
        int high = 0;
        int low = 9;
        int sum = 0;
        for (int a = 0; a<heltal.length;a++){
            if (heltal[a] < low){
                low = heltal[a];
            }
            if (heltal[a] > high){
                high = heltal[a];
            }
            sum += heltal[a];
            System.out.println(heltal[a]); // Iom att den randomiserar numrorna kan det vara kul att kontrollera att resultatet stämmer
        }
        System.out.println("Högsta: "+high+"\n Lägsta: "+low+"\n summa: "+ sum);
    }
}
